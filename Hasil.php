<?php

// require 'functions.php';


$pembeli = $_POST["pembeli"];
$nama_barang = $_POST["nama_barang"];
$jumlah_barang = $_POST["jumlah_barang"];
$uang = $_POST["uang"];


function harga($nama_barang)
{
    if ($nama_barang == 'Baju panjang') {
        $harga = 90000;
    } elseif ($nama_barang == 'Jilbab') {
        $harga = 40000;
    } elseif ($nama_barang == 'Celana Panjang') {
        $harga = 25000;
    } elseif ($nama_barang == 'Gamis Cantik') {
        $harga = 110000;
    } elseif ($nama_barang == 'Rok Panjang') {
        $harga = 50000;
    }
    return $harga;
}
function sebelum_diskon($jumlah_barang, $harga)
{
    $sebelum_diskon = $jumlah_barang * $harga;
    return $sebelum_diskon;
}
function diskon($nama_barang)
{
    if ($nama_barang == 'Baju panjang') {
        $diskon = 0.5;
    } elseif ($nama_barang == 'Jilbab') {
        $diskon = 0.2;
    } elseif ($nama_barang == 'Celana Panjang') {
        $diskon = 0.4;
    } elseif ($nama_barang == 'Gamis Cantik') {
        $diskon = 0.8;
    } elseif ($nama_barang == 'Rok Panjang') {
        $diskon = 0.9;
    }
    return $diskon;
}
function setelah_diskon($diskon, $sebelum_diskon)
{
    $setelah_diskon = $sebelum_diskon * (1 - $diskon);
    return $setelah_diskon;
}
// $setelah_diskon = setelah_diskon(0.2, 20000);
$setelah_diskon = setelah_diskon(diskon($nama_barang), sebelum_diskon($jumlah_barang, harga($nama_barang)));
function kembalian($uang, $setelah_diskon)
{
    $kembalian = $uang - $setelah_diskon;
    return $kembalian;
}
function rupiah($angka)
{
    $hasil = 'Rp. ' . number_format($angka, 2, ",", ".");
    return $hasil;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="Style.css">
</head>

<body>
    <?php
    if ($_POST['uang'] >= setelah_diskon(diskon($nama_barang), sebelum_diskon($jumlah_barang, harga($nama_barang)))) {
    ?>

        <script>
            alert("Anda memiliki kembalian");
            // alert('<?php
            //         echo rupiah(kembalian($uang, setelah_diskon(diskon($nama_barang), sebelum_diskon($jumlah_barang, harga($nama_barang))))) ?>');
            document.location.href = Hasil.php;
        </script>

    <?php } else {
    ?>
        <script>
            alert("Uang anda kurang ");
            // alert('<?php
            //         echo rupiah(kembalian($uang, setelah_diskon(diskon($nama_barang), sebelum_diskon($jumlah_barang, harga($nama_barang))))) ?>');
            document.location.href = beli.php;
        </script>
    <?php }
    ?>
    <!-- <div>
        <svg class="kotak">
            <rect rx="0" ry="0" x="0" y="0" width="500" height="550">
            </rect>
        </svg>
    </div> -->
    <h1>Hasil Pembelian</h1>
    <div class="hasil">
        <b>
            <h4 class="tanggal">
                <td width="60%"></td>
                <td><?php $tgl = date('l, d-m-Y');
                    echo $tgl; ?> </td>
            </h4>
            <h4>Nama Pembeli :
                <?php
                echo ($pembeli);
                ?></h4>

            <h4>Nama Barang :
                <?php
                echo ($nama_barang);
                ?></h4>
            <h4>jumlah_barang :
                <?php
                echo ($jumlah_barang);
                ?></h4>
            <h4>Harga /Barang :
                <?php
                echo rupiah(harga($nama_barang));
                ?>
            </h4>
            <h4> Total Harga Sebelum Diskon :
                <?php
                echo rupiah(sebelum_diskon($jumlah_barang, harga($nama_barang)));
                ?>
            </h4>
            <h4>Total Harga Setelah Diskon :
                <?php
                echo rupiah(setelah_diskon(diskon($nama_barang), sebelum_diskon($jumlah_barang, harga($nama_barang))));
                ?>

            </h4>
            <h4> Total Harga :
                <?php
                echo rupiah(setelah_diskon(diskon($nama_barang), sebelum_diskon($jumlah_barang, harga($nama_barang))));
                ?>
            </h4>
            <h4>Uang yang dibayarkan :
                <?php
                echo rupiah($uang);
                ?></h4>
            <h4>Uang kembalian :
                <?php
                echo rupiah(kembalian($uang, setelah_diskon(diskon($nama_barang), sebelum_diskon($jumlah_barang, harga($nama_barang)))));
                ?>
            </h4>
        </b>

    </div>
    <div class="name">
        <p>Toko Baju Simpel - <a href="">Syi'ta V3420073</a></p>
    </div>
</body>

</html>