<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="Style.css">
</head>

<body>
    <h1>= DAFTAR BARANG YANG TERSEDIA =</h1>

    <br>
    <br>

    <a class="Beli" type="button" href="Beli.php">Beli</a>

    <table border="1" cellpadding=10 cellspancing=0>
        <thead>
            <th>Nama Barang</th>
            <th>Persediaan</th>
            <th>Harga</th>
            <th>Diskon</th>
            <th>Gambar</th>
        </thead>

        <tbody>
            <td>Baju panjang </td>
            <td>100 </td>
            <td>Rp. 90.0000 </td>
            <td>50% </td>
            <td><img src="baju panjang.jfif" alt="" class="foto"></td>
        </tbody>
        <tbody>
            <td>Celana panjang </td>
            <td>50 </td>
            <td>Rp. 40.0000 </td>
            <td>40% </td>
            <td><img src="celana.jpg" alt="" class="foto"></td>
        </tbody>
        <tbody>
            <td>Jilbab </td>
            <td>150 </td>
            <td>Rp. 25.0000 </td>
            <td>20% </td>
            <td><img src="jilbab.jpeg" alt="" class="foto"></td>
        </tbody>
        <tbody>
            <td>Gamis Cantik </td>
            <td>80 </td>
            <td>Rp. 110.0000 </td>
            <td>80% </td>
            <td><img src="gamis.jpg" alt="" class="foto"></td>
        </tbody>
        <tbody>
            <td>Rok panjang </td>
            <td>25 </td>
            <td>Rp. 50.0000 </td>
            <td>90% </td>
            <td><img src="rok.jpg" alt="" class="foto"></td>
        </tbody>
    </table>
    <div class="name">
        <p>Toko Baju Simpel - <a href="">Syi'ta V3420073</a></p>
    </div>
</body>

</html>